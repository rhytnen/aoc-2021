import sys
import argparse
import os
from glob import glob

def run_day_x_script(day):
    if os.path.exists(f'day_{day}'):
        os.chdir(f'day_{day}')
        os.system(f'python day_{day}.py')
        os.chdir('..')


def main():
    parser = argparse.ArgumentParser(description="""Runner script for puzzles""",
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('--day', '-d', default='last', help='Which day to run?  [all | last | 1, 2, etc]')
    args = parser.parse_args(sys.argv[1:])

    if args.day == 'all':
        for i in range(1, 26):
            run_day_x_script(i)
    elif args.day == 'last':
        days = glob('day_*')
        if len(days):
            run_day_x_script(sorted([int(day.split('_')[1]) for day in days])[-1])
        else:
            print('There are no day_x directories to execute.')
    else:
        run_day_x_script(args.day)


if __name__ == '__main__':
    main()
