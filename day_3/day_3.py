from copy import copy


def get_most_common_bits(data: list[str]) -> list[str]:
    bits = [s.strip() for s in data]
    bit_counts = [sum([int(s[i]) for s in bits]) for i in range(len(bits[0]))]

    def mcb(count, max_count):
        return '1' if count >= max_count / 2 else '0'

    return [mcb(i, len(bits)) for i in bit_counts]


def day_3_1(data: list[str]) -> int:
    gamma = ['1' if i == '=' else i for i in get_most_common_bits(data)]
    epsilon = ['0' if g == '1' else '1' for g in gamma]
    return int(''.join(gamma), 2) * int(''.join(epsilon), 2)


def day_3_2(data):
    o2_values = copy(data)
    current_bit = 0
    while len(o2_values) > 1:
        mcb = get_most_common_bits(o2_values)
        o2_values = [v for v in o2_values if (v[current_bit] == mcb[current_bit])]
        current_bit += 1

    co2_values = copy(data)
    current_bit = 0
    while len(co2_values) > 1:
        mcb = get_most_common_bits(co2_values)
        co2_values = [v for v in co2_values if v[current_bit] != mcb[current_bit]]
        current_bit += 1

    return int(''.join(o2_values[0]), 2) * int(''.join(co2_values[0]), 2)


def main():
    data = open('day_3.txt', 'r').readlines()

    print("Day 3")
    print(f"  Puzzle 1: {day_3_1(data)}")
    print(f"  Puzzle 2: {day_3_2(data)}")


if __name__ == "__main__":
    main()
