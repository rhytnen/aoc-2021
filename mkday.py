import argparse
import os
import sys
from glob import glob

python_template = """def day_{0}_1(data):
    ...
    
    
def day_{0}_2(data):
    ...
   
   
def main():
    data = open('day_{0}.txt', 'r').readlines()

    print("Day {0}")
    print(f"  Puzzle 1: {{day_{0}_1(data)}}")
    print(f"  Puzzle 2: {{day_{0}_2(data)}}")


if __name__ == "__main__":
    main()

"""

readme_template = """# 2021 Day {0}
#### [Direct Link](https://adventofcode.com/2021/day/{0}

## Part 1

## Part 2

"""


def mk_day(day: int):
    if day == 0:
        days = glob('day_*')
        if len(days):
            day = sorted([int(day.split('_')[1]) for day in days])[-1] + 1
        else:
            day = 1

    d = f'day_{day}'
    os.mkdir(d)
    with open(f'{d}/{d}.py', 'w') as py:
        py.write(python_template.format(day))

    with open(f'{d}/README.md', 'w') as readme:
        readme.write(readme_template.format(day))

    open(f'{d}/{d}.txt', 'w').close()


def main():
    parser = argparse.ArgumentParser(description="""Make a new folder for a aoc puzzle""",
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('--day', '-d', default=0, type=int, help='Which day to create [1, 2, etc]')
    args = parser.parse_args(sys.argv[1:])
    mk_day(args.day)


if __name__ == '__main__':
    main()
