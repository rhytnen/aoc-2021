def calculate_bingo(card: list[list[int]]) -> bool:
    for row in card:
        if sum(row) == -5:
            return True

    for i in range(len(card[0])):
        col = [row[i] for row in card]
        if sum(col) == -5:
            return True

    return False


def mark_card(card: list[list[int]], number: int):
    for row in card:
        try:
            index = row.index(number)
            row[index] = -1
        except ValueError:
            ...


def sum_card(card: list[list[int]]) -> int:
    total = 0
    for row in card:
        for col in row:
            if col != -1:
                total += col
    return total


def parse_data(data):
    numbers = [int(i) for i in data[0].split(',')]
    card_str = [l.strip().split(' ') for l in data[1:] if len(l.strip()) > 0]
    card_data = []
    for card in card_str:
        card_data.append([int(x.strip()) for x in card if x])
    cards = [card_data[i:i + 5] for i in range(0, len(card_data), 5)]

    return numbers, cards


def day_4_1(data):
    numbers, cards = parse_data(data)
    for n in numbers:
        for card in cards:
            mark_card(card, n)
            if calculate_bingo(card):
                return sum_card(card) * n


def day_4_2(data):
    numbers, cards = parse_data(data)
    for n in numbers:
        pop = []
        for card in cards:
            mark_card(card, n)
            if calculate_bingo(card):
                pop.append(card)

        if len(pop) == 1 and len(cards) == 1:
            return sum_card(pop[0]) * n

        for p in pop:
            cards.remove(p)


def main():
    data = open('day_4.txt', 'r').readlines()

    print("Day 4")
    print(f"  Puzzle 1: {day_4_1(data)}")
    print(f"  Puzzle 2: {day_4_2(data)}")


if __name__ == "__main__":
    main()
