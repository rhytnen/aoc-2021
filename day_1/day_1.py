def day_1_1(depths: list[int]) -> int:
    return len([i for i in [y - x for x, y in zip(depths[:-1], depths[1:])] if i > 0])


def day_1_2(depths_avg: list[int]) -> int:
    return day_1_1([sum(depths_avg[i:i + 3]) for i in range(0, len(depths_avg) - 2)])


def main():
    depths = [int(i) for i in open('day_1.txt', 'r').readlines()]

    print("Day 1")
    print(f"  Puzzle 1: {day_1_1(depths)}")
    print(f"  Puzzle 2: {day_1_2(depths)}")


if __name__ == "__main__":
    main()
