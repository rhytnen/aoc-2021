def day_2_1(controls: list[str]) -> int:
    horiz = 0
    depth = 0

    for control, value in [c.split(' ') for c in controls]:
        value = int(value)
        if control == 'forward':
            horiz += value
        elif control == 'down':
            depth += value
        elif control == 'up':
            depth -= value
            if depth < 0:
                print('Error: depth out of range.')
                exit(0)
        else:
            print(f'Error: unknown control: {control}')
            exit(0)
    return horiz * depth


def day_2_2(controls: list[str]) -> int:
    horiz = 0
    depth = 0
    aim = 0
    for control, value in [c.split(' ') for c in controls]:
        value = int(value)
        if control == 'forward':
            horiz += value
            depth += aim * value
        elif control == 'down':
            aim += value
        elif control == 'up':
            aim -= value
        else:
            print(f'Error: unknown control: {c}')
            exit(0)
    return horiz * depth


def main():
    controls = open('day_2.txt', 'r').readlines()

    print("Day 2")
    print(f"  Puzzle 1: {day_2_1(controls)}")
    print(f"  Puzzle 2: {day_2_2(controls)}")


if __name__ == "__main__":
    main()
